<?php
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = [
    'id'=>'yii2_simple_web',
    'defaultRoute'=>'index/index',
    'basePath'=>dirname(__DIR__),
    'controllerNamespace'=>'alexs\tests\controllers',
    'layoutPath' => __DIR__ . '/views/layouts',
    'components'=>[
        'db'=>[
            'class'=>'yii\db\Connection',
            'dsn'=>'mysql:host=localhost;dbname=yii2_simple_web',
            'username'=>'root',
            'password'=>'',
            'charset'=>'utf8',
        ],
        'urlManager'=>[
            'enablePrettyUrl'=>true,
            'showScriptName'=>false,
            'rules'=>[

            ],
        ],
    ],
];

(new yii\web\Application($config))->run();