<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   06.09.2017
 */

namespace alexs\tests\controllers;
use yii\web\Controller;

class IndexController extends Controller
{
    public function getViewPath() {
        return __DIR__ . '/../views/index';
    }

    public function actionIndex() {
        return $this->render('index', [

        ]);
    }
}